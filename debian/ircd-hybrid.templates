# These templates have been reviewed by the debian-l10n-english
# team
#
# If modifications/additions/rewording are needed, please ask
# debian-l10n-english@lists.debian.org for advice.
#
# Even minor modifications require translation updates and such
# changes should be coordinated with translators and reviewers.

Template: ircd-hybrid/restart_on_upgrade
Type: boolean
Default: true
_Description: Restart ircd-hybrid on each upgrade?
 Please choose whether the ircd-hybrid daemon should be restarted
 every time a new version of this package is installed.
 .
 Automatic restarts may be problematic if, for instance, the server is
 running with manually loaded modules, which will need to be reloaded
 after the restart.
 .
 If you reject this option, you will have to restart ircd-hybrid via
 "service ircd-hybrid restart" when needed.

Template: ircd-hybrid/automatically_fix_ssl_config
Type: boolean
Default: false
_Description: Automatically fix references to obsolete ssl config options?
 Several ssl configuration variables have been renamed to tls-like ones in
 version 8.2.30. If enabled, the post-installation script will attempt to
 automatically fix them before the server is restarted. If not, and you
 have any ssl options specified, the configuration will become invalid,
 and server restart will fail.
