Source: ircd-hybrid
Section: net
Priority: optional
Build-Depends: debhelper-compat (= 12),
 docbook-to-man,
 flex,
 bison,
 automake (>= 1:1.13.1),
 libltdl-dev,
 libgnutls28-dev (>= 3.6.5),
Build-Conflicts: autoconf2.13
Maintainer: Dominic Hargreaves <dom@earth.li>
Standards-Version: 4.1.4
Homepage: http://ircd-hybrid.com/
Vcs-Browser: https://salsa.debian.org/dom/ircd-hybrid
Vcs-Git: https://salsa.debian.org/dom/ircd-hybrid.git

Package: ircd-hybrid
Architecture: any
Conflicts: ircd-ircu, ircd-irc2, dancer-ircd, oftc-hybrid
Pre-Depends: debconf (>= 0.5) | debconf-2.0
Depends: ${shlibs:Depends},
    ${misc:Depends},
    openssl,
    lsb-base (>= 3.0-6),
Provides: ircd
Recommends: whois
Suggests: anope
Description: high-performance secure IRC server
 ircd-hybrid is a stable, high-performance IRC server that features:
 .
  * SSL client support and server-to-server RSA encryption;
  * Compressed server links;
  * Channel exceptions (+e) and invitation exceptions (+I);
  * New configuration file format;
  * Halfops (+h) and anti-spam user mode +g;
  * Dynamically loadable modules;
  * Channel and nickname RESV's (reservations).

Package: hybrid-dev
Section: devel
Architecture: all
Depends: ${misc:Depends}
Suggests: ircd-hybrid
Description: high-performance secure IRC server - development files
 These are the headers used when writing modules for ircd-hybrid.
 For more information on how to write these modules, see the ircd-hybrid
 documentation or example_module.c in the source code for ircd-hybrid.
 .
 It also includes mbuild-hybrid, a shell script that simplifies building 
 and installation of such modules. This shell script is simplistic and
 assumes a lot; if you possess clue, you will know what to do anyway.
